Source: lunar-date
Priority: optional
Maintainer: Debian Chinese Team <chinese-developers@lists.alioth.debian.org>
Uploaders:
 YunQiang Su <syq@debian.org>,
 ChangZhuo Chen (陳昌倬) <czchen@debian.org>,
 xiao sheng wen <atzlinux@sina.com>,
Build-Depends:
 autoconf-archive,
 debhelper-compat (= 13),
 gnome-common,
 gobject-introspection,
 gtk-doc-tools (>= 1.0),
 intltool,
 libgirepository1.0-dev,
 libglib2.0-dev,
 valac,
 dh-sequence-gir,
Rules-Requires-Root: no
Standards-Version: 4.5.0
Section: libs
Homepage: https://code.google.com/p/liblunar/
Vcs-Git: https://salsa.debian.org/chinese-team/lunar-date.git
Vcs-Browser: https://salsa.debian.org/chinese-team/lunar-date

Package: gir1.2-lunar-date-2.0
Section: introspection
Multi-Arch: same
Architecture: any
Depends:
 ${gir:Depends},
 ${misc:Depends},
Description: GObject Introspection for lunar-date
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains GObject Introspection for lunar-date.

Package: liblunar-date-2.0-0
Pre-Depends:
 ${misc:Pre-Depends},
Multi-Arch: same
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Chinese Lunar library based on GObject
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains shared library and vala bindings.

Package: liblunar-date-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblunar-date-2.0-0 (= ${binary:Version}),
 ${misc:Depends},
Replaces: gir1.2-lunar-date-2.0 (<< 2.4.0-7)
Breaks: gir1.2-lunar-date-2.0 (<< 2.4.0-7)
Description: Chinese Lunar library based on GObject - develop files
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains develop files: C headers and static library.

Package: liblunar-date-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Chinese Lunar library based on GObject - API documents
 Lunar-date is a Chinese Lunar library based on GObject, which can
 covert between Chinese lunar calendar and Gregorian calendar.
 .
 This package contains API documents.
