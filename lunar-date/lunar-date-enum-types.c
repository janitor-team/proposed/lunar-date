
/* Generated data (by glib-mkenums) */

#include "lunar-date-enum-types.h"

/* enumerations from "./lunar-date.h" */
#include "./lunar-date.h"

GType
lunar_date_error_get_type (void)
{
	static GType the_type = 0;
	
	if (the_type == 0)
	{
		static const GEnumValue values[] = {
			{ LUNAR_DATE_ERROR_INTERNAL,
			  "LUNAR_DATE_ERROR_INTERNAL",
			  "internal" },
			{ LUNAR_DATE_ERROR_YEAR,
			  "LUNAR_DATE_ERROR_YEAR",
			  "year" },
			{ LUNAR_DATE_ERROR_MONTH,
			  "LUNAR_DATE_ERROR_MONTH",
			  "month" },
			{ LUNAR_DATE_ERROR_DAY,
			  "LUNAR_DATE_ERROR_DAY",
			  "day" },
			{ LUNAR_DATE_ERROR_HOUR,
			  "LUNAR_DATE_ERROR_HOUR",
			  "hour" },
			{ LUNAR_DATE_ERROR_LEAP,
			  "LUNAR_DATE_ERROR_LEAP",
			  "leap" },
			{ 0, NULL, NULL }
		};
		the_type = g_enum_register_static (
				g_intern_static_string ("LunarDateError"),
				values);
	}
	return the_type;
}


/* Generated data ends here */

